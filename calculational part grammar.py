    @_('expr PLUS term')
    def expr(self, p):
        return p.expr + p.term

    @_('expr MINUS term')
    def expr(self, p):
        return p.expr - p.term
    @_('term')
    def expr(self, p):
        return p.term

    @_('term TIMES factor')
    def term(self, p):
        return p.term * p.factor

    @_('term DIVIDE factor')
    def term(self, p):
        return p.term / p.factor

    @_('factor')
    def term(self, p):
        return p.factor

    @_('NUMBER')
    def factor(self, p):
        print(p.NUMBER)
        return p.NUMBER
    @_('ID')
    def factor(self, p):
        print(p.ID)
        return p.ID
    @_('term POWER factor')
    def term(self,p):
        return p.term ** p.factor

    @_('LPAR expr RPAR')
    def factor(self, p):
        return p.expr