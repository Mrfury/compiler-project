class symbtable():
        def __init__(self,symbtab):
            self.symbtab = symbtab
        def free(self):
            self.symbtab = []
        def lookup(self,t):
            temp = False
            for i in self.symbtab:
                if t.value in i.value:
                    temp = True
                    break
                else:
                    temp = False
            return temp
